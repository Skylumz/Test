﻿namespace CodeWalker
{
    partial class TestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.info_WF1 = new CodeWalker.UserControls.Info_WF();
            this.SuspendLayout();
            // 
            // info_WF1
            // 
            this.info_WF1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.info_WF1.Location = new System.Drawing.Point(131, 35);
            this.info_WF1.Name = "info_WF1";
            this.info_WF1.Size = new System.Drawing.Size(229, 552);
            this.info_WF1.TabIndex = 0;
            // 
            // TestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(581, 608);
            this.Controls.Add(this.info_WF1);
            this.Name = "TestForm";
            this.Text = "TestForm";
            this.ResumeLayout(false);

        }

        #endregion

        private UserControls.Info_WF info_WF1;
    }
}