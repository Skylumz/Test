﻿namespace CodeWalker.UserControls
{
    partial class Info_WF
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Info_BTN = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.MouseSelectCheckBox = new System.Windows.Forms.CheckBox();
            this.SelectionNameTextBox = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.SelEntityPropertyGrid = new System.Windows.Forms.PropertyGrid();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.propertyGrid3 = new System.Windows.Forms.PropertyGrid();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.propertyGrid2 = new System.Windows.Forms.PropertyGrid();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.ModeComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // Info_BTN
            // 
            this.Info_BTN.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Info_BTN.Location = new System.Drawing.Point(182, 3);
            this.Info_BTN.Name = "Info_BTN";
            this.Info_BTN.Size = new System.Drawing.Size(34, 23);
            this.Info_BTN.TabIndex = 0;
            this.Info_BTN.Text = "I";
            this.Info_BTN.UseVisualStyleBackColor = true;
            this.Info_BTN.Click += new System.EventHandler(this.Info_BTN_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.MouseSelectCheckBox);
            this.panel1.Controls.Add(this.SelectionNameTextBox);
            this.panel1.Controls.Add(this.tabControl1);
            this.panel1.Controls.Add(this.ModeComboBox);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(10, 32);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(209, 510);
            this.panel1.TabIndex = 1;
            this.panel1.Visible = false;
            // 
            // MouseSelectCheckBox
            // 
            this.MouseSelectCheckBox.AutoSize = true;
            this.MouseSelectCheckBox.Location = new System.Drawing.Point(4, 3);
            this.MouseSelectCheckBox.Name = "MouseSelectCheckBox";
            this.MouseSelectCheckBox.Size = new System.Drawing.Size(161, 19);
            this.MouseSelectCheckBox.TabIndex = 23;
            this.MouseSelectCheckBox.Text = "Mouse select (right click)";
            this.MouseSelectCheckBox.UseVisualStyleBackColor = true;
            this.MouseSelectCheckBox.CheckedChanged += new System.EventHandler(this.MouseSelectCheckBox_CheckedChanged);
            // 
            // SelectionNameTextBox
            // 
            this.SelectionNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SelectionNameTextBox.BackColor = System.Drawing.Color.White;
            this.SelectionNameTextBox.Location = new System.Drawing.Point(1, 67);
            this.SelectionNameTextBox.Name = "SelectionNameTextBox";
            this.SelectionNameTextBox.ReadOnly = true;
            this.SelectionNameTextBox.Size = new System.Drawing.Size(199, 20);
            this.SelectionNameTextBox.TabIndex = 27;
            this.SelectionNameTextBox.Text = "Nothing selected";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabControl1.Location = new System.Drawing.Point(0, 93);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(209, 417);
            this.tabControl1.TabIndex = 4;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.SelEntityPropertyGrid);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(201, 391);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Entity";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // SelEntityPropertyGrid
            // 
            this.SelEntityPropertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SelEntityPropertyGrid.HelpVisible = false;
            this.SelEntityPropertyGrid.Location = new System.Drawing.Point(0, 0);
            this.SelEntityPropertyGrid.Name = "SelEntityPropertyGrid";
            this.SelEntityPropertyGrid.PropertySort = System.Windows.Forms.PropertySort.NoSort;
            this.SelEntityPropertyGrid.Size = new System.Drawing.Size(201, 391);
            this.SelEntityPropertyGrid.TabIndex = 35;
            this.SelEntityPropertyGrid.ToolbarVisible = false;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.propertyGrid3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(201, 391);
            this.tabPage1.TabIndex = 1;
            this.tabPage1.Text = "Archetype";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // propertyGrid3
            // 
            this.propertyGrid3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGrid3.HelpVisible = false;
            this.propertyGrid3.Location = new System.Drawing.Point(0, 0);
            this.propertyGrid3.Name = "propertyGrid3";
            this.propertyGrid3.PropertySort = System.Windows.Forms.PropertySort.NoSort;
            this.propertyGrid3.Size = new System.Drawing.Size(201, 391);
            this.propertyGrid3.TabIndex = 36;
            this.propertyGrid3.ToolbarVisible = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.propertyGrid2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(201, 391);
            this.tabPage2.TabIndex = 2;
            this.tabPage2.Text = "Drawable";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // propertyGrid2
            // 
            this.propertyGrid2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.propertyGrid2.HelpVisible = false;
            this.propertyGrid2.Location = new System.Drawing.Point(0, 11);
            this.propertyGrid2.Name = "propertyGrid2";
            this.propertyGrid2.PropertySort = System.Windows.Forms.PropertySort.NoSort;
            this.propertyGrid2.Size = new System.Drawing.Size(202, 216);
            this.propertyGrid2.TabIndex = 36;
            this.propertyGrid2.ToolbarVisible = false;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.propertyGrid1);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(201, 391);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Ext";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.propertyGrid1.HelpVisible = false;
            this.propertyGrid1.Location = new System.Drawing.Point(-1, 11);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.PropertySort = System.Windows.Forms.PropertySort.NoSort;
            this.propertyGrid1.Size = new System.Drawing.Size(202, 216);
            this.propertyGrid1.TabIndex = 36;
            this.propertyGrid1.ToolbarVisible = false;
            // 
            // ModeComboBox
            // 
            this.ModeComboBox.FormattingEnabled = true;
            this.ModeComboBox.Items.AddRange(new object[] {
            "Entity",
            "Entity Extension",
            "Archetype Extension",
            "Time Cycle Modifier",
            "Car Generator",
            "Grass",
            "Water Quad",
            "Collision",
            "Nav Mesh",
            "Path",
            "Train Track",
            "Distant Lod Lights",
            "Mlo Instance",
            "Scenario",
            "Audio"});
            this.ModeComboBox.Location = new System.Drawing.Point(49, 34);
            this.ModeComboBox.Name = "ModeComboBox";
            this.ModeComboBox.Size = new System.Drawing.Size(151, 21);
            this.ModeComboBox.TabIndex = 2;
            this.ModeComboBox.SelectedIndexChanged += new System.EventHandler(this.ModeComboBox_SelectedIndexChanged);
            this.ModeComboBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ModeComboBox_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Mode:";
            // 
            // Info_WF
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.Info_BTN);
            this.Name = "Info_WF";
            this.Size = new System.Drawing.Size(229, 552);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Info_BTN;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox ModeComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.PropertyGrid propertyGrid3;
        private System.Windows.Forms.PropertyGrid propertyGrid2;
        private System.Windows.Forms.PropertyGrid propertyGrid1;
        private System.Windows.Forms.TextBox SelectionNameTextBox;
        private System.Windows.Forms.CheckBox MouseSelectCheckBox;
        public System.Windows.Forms.PropertyGrid SelEntityPropertyGrid;
    }
}
