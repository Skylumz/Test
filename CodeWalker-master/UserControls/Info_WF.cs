﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeWalker.UserControls
{
    public partial class Info_WF : UserControl
    {
        public WorldForm WorldF = new WorldForm();
        public Info_WF()
        {
            InitializeComponent();
        }

        public void setWorldForm(WorldForm wf)
        {
            WorldF = wf;
        }

        private void Info_BTN_Click(object sender, EventArgs e)
        {
            if (panel1.Visible == false)
            {
                panel1.Visible = true;
            }
            else
            {
                panel1.Visible = false;
            }
        }

        private void MouseSelectCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if(WorldF != null)
            {
                WorldF.MouseSelectCheckBox_CheckedChanged(sender, e);
            }
        }

        private void ModeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (WorldF != null)
            {
                WorldF.SelectionModeComboBox_SelectedIndexChanged(sender, e);
            }
        }

        private void ModeComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (WorldF != null)
            {
                WorldF.SelectionModeComboBox_KeyPress(sender, e); 
            }
        }
    }
}
